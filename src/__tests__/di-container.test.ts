import { DIContainer, UnknownDependencyError, FactoryError } from '..';

class Dependency {
  private readonly _val: number;

  constructor(val: number = 0, public name?: string) {
    this._val = val;
  }

  get val() {
    return this._val;
  }
}

class SubDependency {
  constructor(public readonly d: Dependency) {}
}

const dependencyFunction = () => 'Europa';

describe('DIContainer', () => {
  it('registers object dependencies', async () => {
    const c = new DIContainer();
    expect(c.register(Dependency, () => new Dependency())).toEqual(c);
  });

  it('registers string dependencies', async () => {
    const c = new DIContainer();
    expect(c.register('pi', () => 3.141)).toEqual(c);
  });

  it('builds object dependencies', async () => {
    const c = new DIContainer();
    c.register(Dependency, () => new Dependency(27));

    const d = await c.get<Dependency>(Dependency);
    expect(d).toBeInstanceOf(Dependency);
    expect(d.val).toBe(27);
  });

  it('returns scalar dependencies', async () => {
    const c = new DIContainer();
    c.register('pi', () => 3.14);

    const pi = await c.get<number>('pi');
    expect(pi).toBe(3.14);
  });

  it('returns functional dependencies', async () => {
    const c = new DIContainer();
    c.register('jupiterMoon', () => dependencyFunction);

    const f = await c.get<typeof dependencyFunction>('jupiterMoon');
    expect(f).toEqual(dependencyFunction);
    expect(f()).toEqual(dependencyFunction());
  });

  it('can register function names as dependency keys', async () => {
    const c = new DIContainer();
    c.register(dependencyFunction, () => dependencyFunction);

    const f = await c.get<typeof dependencyFunction>(dependencyFunction);
    expect(f).toEqual(dependencyFunction);
    expect(f()).toEqual(dependencyFunction());
  });

  it('will build multi-dependency objects', async () => {
    const c = new DIContainer();
    c.register('dbPort', () => 8080);
    c.register(Dependency, async co => new Dependency(await co.get('dbPort')));
    c.register(SubDependency, async co => new SubDependency(await co.get(Dependency)));

    const subDep = await c.get<SubDependency>(SubDependency);
    expect(subDep.d).toBeInstanceOf(Dependency);
    expect(subDep.d.val).toBe(8080);
  });

  it('will overwrite dependencies', async () => {
    const c = new DIContainer();

    c.register('myDep', () => new Dependency(27));
    expect(await c.get('myDep')).toBeInstanceOf(Dependency);

    c.register('myDep', () => 55);
    expect(await c.get('myDep')).toBe(55);
  });

  it('will accept arguments to the factory function', async () => {
    const c = new DIContainer();
    c.register(Dependency, (co, num: number, name: string) => new Dependency(num, name));

    const i1 = await c.get<Dependency>(Dependency, 27, 'Aeris');
    const i2 = await c.get<Dependency>(Dependency, 55, 'Cloud');

    expect(i1.val).toBe(27);
    expect(i1.name).toBe('Aeris');

    expect(i2.val).toBe(55);
    expect(i2.name).toBe('Cloud');
  });

  it('can clear the built objects cache', async () => {
    const c = new DIContainer();
    c.register(Dependency, (co, num: number) => new Dependency(num));

    const i1 = await c.get<Dependency>(Dependency);
    i1.name = 'Cid';

    c.clearBuiltCache();

    const i2 = await c.get<Dependency>(Dependency);
    expect(i1.name).not.toBe(i2.name);
  });

  describe('Error handling', () => {
    it('will throw UnknownDependency if the factory does not exist', async () => {
      const c = new DIContainer();
      await expect(c.get(Dependency)).rejects.toThrow(UnknownDependencyError);
    });

    it('will throw FactoryError if the factory throws', async () => {
      const errToThrow = new Error('Something is terribly wrong');

      const c = new DIContainer();
      c.register(Dependency, () => {
        throw errToThrow;
      });

      let err: any;
      try {
        await c.get(Dependency);
      } catch (e) {
        err = e;
      }

      expect(err).toBeInstanceOf(FactoryError);
      expect(err.originalError).toEqual(errToThrow);
      expect(err.message).toBe(`Error occurred during dependency "Dependency" construction: ${errToThrow.message}`);
    });
  });
});
