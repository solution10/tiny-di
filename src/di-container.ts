/**
 * DIContainer
 *
 * A tiny DI/IOC container based heavily on the awesome PHP DI container Pimple.
 *
 * @author  Alex Gisby <alex@solution10.com>
 * @license MIT
 */

export abstract class DIContainerError extends Error {}

export class UnknownDependencyError extends DIContainerError {
  public name = 'UnknownDependencyError';
}

export class FactoryError extends DIContainerError {
  public name = 'FactoryError';

  constructor(dependency: string, public readonly originalError: Error) {
    super(`Error occurred during dependency "${dependency}" construction: ${originalError.message}`);
    this.stack = originalError.stack;
  }
}

type Dictionary<T> = { [k: string]: T };
export type DIFactory = (container: DIContainer, ...args: Array<any>) => any | Promise<any>;

type BuiltCache = Dictionary<{
  [argsSerialized: string]: any;
}>;

export class DIContainer {
  protected readonly factories: Dictionary<DIFactory> = {};
  protected built: BuiltCache = {};

  register<T>(constructor: T, factory: DIFactory): DIContainer {
    const key = DIContainer.keyFromConstructor(constructor);
    this.factories[key] = factory;
    this.built[key] = {};

    return this;
  }

  async get<T>(constructor: any, ...args: Array<any>): Promise<T> {
    const key = DIContainer.keyFromConstructor(constructor);
    const argsKey = DIContainer.serializeArguments(args);

    if (!this.factories[key]) {
      throw new UnknownDependencyError(`No dependency registered for ${key}`);
    }

    if (!this.built[key]) {
      this.built[key] = {};
    }

    if (!this.built[key][argsKey]) {
      try {
        this.built[key][argsKey] = await this.factories[key](this, ...args);
      } catch (e) {
        throw new FactoryError(key, e);
      }
    }

    return this.built[key][argsKey];
  }

  clearBuiltCache(): void {
    this.built = {};
  }

  private static keyFromConstructor<T extends Object | string>(constructor: T): string {
    if (typeof constructor === 'string') {
      return constructor;
    }

    return (constructor as any).name;
  }

  private static serializeArguments(args: Array<any>): string {
    return JSON.stringify(args);
  }
}
